from random import randint
from sys import exit
from collections import namedtuple

# TODO: Напиши документацию к классу и методам, поищи автогенератор документации для твоей IDE

class Warrior:

    def __init__(self, warrior_info: namedtuple):
        self.__name = warrior_info.name
        self.__health = int(warrior_info.health)
        self.__damage = int(warrior_info.damage)
        self.__weapon = Weapon(warrior_info.weapon)
        self.__damage += self.__weapon.get_damage()

    def is_alive(self) -> bool:
        return True if self.__health > 0 else False

    def decrease_hp(self, damage: int):
        self.__health = self.__health - damage if self.__health >= damage else 0

    def attack(self, enemy: 'Warrior'):
        print(self.__name, 'attacks')
        enemy.decrease_hp(self.__damage)
        print(enemy.__name, 'has', enemy.__health, 'helth', end='\n\n')

    def fight(self, enemy: 'Warrior'):
        while self.is_alive() and enemy.is_alive():
            attack = randint(0, 1)
            if attack == 0:
                self.attack(enemy)
            elif attack == 1:
                enemy.attack(self)
        if self.is_alive():
            print(self.__name, 'win')
        else:
            print(enemy.__name, 'win')


class Weapon:

    def __init__(self, type: str):
        self.__type = type
        if self.__type == 'bow':
            self.__damage = 5
        elif self.__type == 'sword':
            self.__damage = 15
        elif self.__type == 'staff':
            self.__damage = 10
        else:
            exit('Unkown type weapon')

    def get_damage(self) -> int: return self.__damage


if __name__ == '__main__':
    warrior_info = namedtuple('warrior_info', ['name', 'health', 'damage', 'weapon'])
    l = input('Enter name, health, damage and weapon(bow, sword, staff)'
                             ' for first warrior \n').split()
    warrior1 = Warrior(warrior_info(l[0], l[1], l[2], l[3]))
    l = input('Enter name, health, damage and weapon(bow, sword, staff)'
              ' for second warrior \n').split()
    warrior2 = Warrior(warrior_info(l[0], l[1], l[2], l[3]))
    warrior1.fight(warrior2)
